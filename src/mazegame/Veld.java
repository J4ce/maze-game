package mazegame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;

public class Veld extends JPanel
{
    private HoofdScherm hs;
    private Doolhof doolhof;
    private int[][] maze;
    private int mazeBlockSize = 25;

    public Veld(Doolhof doolhof)
    {
        this.doolhof = doolhof;
        maze = doolhof.getMaze();
        
        //get current HoofdScherm:
        for(Window w : Window.getWindows())
        {
            if(w.getName().equals("HoofdSchermMain"))
            {
                hs = (HoofdScherm) w;
            }
        }
    }
    
    public int getMazeBlockSize() { return mazeBlockSize; }
    public void setMazeBlockSize(int mazeBlockSize) { this.mazeBlockSize = mazeBlockSize; }
    
    public void calcMazeBlockSize(int w, int h, JPanel container)
    {
        Rectangle r = container.getBounds();
        int mw = (int) Math.round(r.width / w) - 1;
        int mh = (int) Math.round(r.height / h) - 1;
        System.out.format("mw = %3d, mh = %3d\n", mw, mh);
        mazeBlockSize = Math.min(mw, mh);
    }

    private static BufferedImage scale(BufferedImage src, int w, int h)
    {
        int type = BufferedImage.TYPE_INT_RGB;
        BufferedImage dst = new BufferedImage(w, h, type);
        Graphics2D g2 = dst.createGraphics();
        // Fill background for scale to fit.
        g2.setBackground(UIManager.getColor("Panel.background"));
        g2.clearRect(0, 0, w, h);
        double xScale = (double) w / src.getWidth();
        double yScale = (double) h / src.getHeight();
        // Scaling options:
        // Scale to fit - image just fits in label.
        double scale = Math.min(xScale, yScale);
        // Scale to fill - image just fills label.
        //double scale = Math.max(xScale, yScale);
        int width = (int) (scale * src.getWidth());
        int height = (int) (scale * src.getHeight());
        int x = (w - width) / 2;
        int y = (h - height) / 2;
        g2.drawImage(src, x, y, width, height, null);
        g2.dispose();
        return dst;
    }

    public void updateMaze(VeldItem item)
    {
        JPanel container = hs.getMazeContainer();
        Component[] test = container.getComponents();//this.jPanel4.getComponents();

        int count = 0;
        boolean itemCur = false;
        boolean itemPrev = false;
        do
        {
            Component component = test[count];

            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.NORTHWEST;
            c.fill = GridBagConstraints.BOTH;

            String[] parts = component.getName().split(":");
            int part1 = Integer.parseInt(parts[0]);
            int part2 = Integer.parseInt(parts[1]);
            c.gridx = part2;
            c.gridy = part1;

            if (part1 == item.getCurrentPosY() && part2 == item.getCurrentPosX())
            {
                JPanel pnl = new Veld.DrawAnimatedPlayer();
                pnl.setName(part1 + ":" + part2);
                pnl.setOpaque(true);
                container.remove(count);
                container.add(pnl, c, count);
                
                itemCur = true;
                
            } else if (part1 == item.getPrevPosY() && part2 == item.getPrevPosX())
            {
                JPanel pnl = new Veld.DrawPath();
                pnl.setName(part1 + ":" + part2);
                pnl.setOpaque(true);
                container.remove(count);
                container.add(pnl, c, count);
                
                itemPrev = true;
            }
            count++;

        } while (itemCur == false || itemPrev == false);
        
        container.revalidate();
        container.repaint();
        container.updateUI();
    }
    
    public void drawMaze()
    {
        int w = doolhof.getWidth();
        int h = doolhof.getHeight();
        maze = doolhof.getMaze();
        
        JPanel container = hs.getMazeContainer();
        container.removeAll();
        container.updateUI();
        container.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.BOTH;

        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                c.gridx = j;
                c.gridy = i;
                if(maze[i][j] == 1)      //wall
                { 
                    JPanel pnl = new Veld.DrawWall();
                    pnl.setName(i + ":" + j);
                    pnl.setOpaque(true);
                    container.add(pnl, c);
                } 
                else if(maze[i][j] == 0) //path
                {
                    JPanel pnl = new Veld.DrawPath();
                    pnl.setName(i + ":" + j);
                    pnl.setOpaque(true);
                    container.add(pnl, c);
                }
                else if(maze[i][j] == 2) //player
                {
                    JPanel pnl = new Veld.DrawAnimatedPlayer();
                    pnl.setName(i + ":" + j);
                    pnl.setOpaque(true);
                    container.add(pnl, c);
                }
                else if(maze[i][j] == 9) //optimal route
                {
                    JPanel pnl = new Veld.DrawOptimalPath();
                    pnl.setName(i + ":" + j);
                    pnl.setOpaque(true);
                    container.add(pnl, c);
                }
            }

        }
            printMaze();
    }
    
    public void printMaze(){
        int w = doolhof.getWidth();
        int h = doolhof.getHeight();
        System.out.println("Doolhof class maze array:");
        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                System.out.format("%2d",maze[i][j]);
            }
            System.out.println("");
        }
    }
    
    public static class StartScreen extends JPanel
    {

        private BufferedImage image;

        public StartScreen()
        {
            try
            {
                image = ImageIO.read(new File("./startscherm.png"));
            } catch (IOException ex)
            {
                System.out.println(ex);
                System.out.println("Working Directory = " + System.getProperty("user.dir"));
            }
        }

        @Override
        public void paint(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.BLACK);
            g2d.drawImage(image, 0, 0, null);
        }
    }

    @SuppressWarnings("serial")
    public class DrawWall extends JPanel
    {

        public DrawWall()
        {
            super.setSize(mazeBlockSize, mazeBlockSize);
            super.setPreferredSize(new Dimension(mazeBlockSize, mazeBlockSize));
        }

        @Override
        public void paint(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.GRAY);
            g2d.drawRect(0, 0, mazeBlockSize, mazeBlockSize);
            g2d.setColor(Color.DARK_GRAY);
            g2d.fillRect(1, 1, mazeBlockSize - 2, mazeBlockSize - 2);
        }
    }

    @SuppressWarnings("serial")
    public class DrawPath extends JPanel
    {

        private BufferedImage image;

        public DrawPath()
        {
            try
            {
                image = ImageIO.read(new File("./Dungeon_Floor_1.png"));
            } catch (IOException ex)
            {
                System.out.println(ex);
                System.out.println("Working Directory = "
                        + System.getProperty("user.dir"));
            }
        }

        @Override
        public void paint(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.BLACK);
            g2d.drawImage(scale(image, mazeBlockSize, mazeBlockSize), 0, 0, null);
        }
    }

    public class DrawPlayer extends JPanel
    {

        private BufferedImage image;

        public DrawPlayer()
        {
            try
            {
                image = ImageIO.read(new File("./Dungeon_Floor_1.png"));
            } catch (IOException ex)
            {
                System.out.println(ex);
                System.out.println("Working Directory = "
                        + System.getProperty("user.dir"));
            }
        }

        @Override
        public void paint(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
//            g2d.setColor(Color.LIGHT_GRAY);
//            g2d.fillRect(0, 0, getWidth(), getHeight());
            g2d.drawImage(scale(image, mazeBlockSize, mazeBlockSize), 0, 0, null);
            g2d.setColor(Color.RED);
            //g2d.fillOval(getWidth() / 6, getHeight() / 6, (int)Math.round(getWidth() / 1.5), (int)Math.round(getHeight() / 1.5));
            g2d.fillOval(0, 0, getWidth(), getHeight());
            g2d.setColor(Color.PINK);
            g2d.drawOval(0, 0, getWidth(), getHeight());
        }
    }

    @SuppressWarnings("serial")
    public class DrawOptimalPath extends JPanel
    {

        private BufferedImage image;

        public DrawOptimalPath()
        {
            try
            {
                image = ImageIO.read(new File("./Dungeon_Floor_1.png"));
            } catch (IOException ex)
            {
                System.out.println(ex);
                System.out.println("Working Directory = "
                        + System.getProperty("user.dir"));
            }
        }

        @Override
        public void paint(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(scale(image, mazeBlockSize, mazeBlockSize), 0, 0, null);
            g2d.setColor(Color.YELLOW);
            g2d.drawRect(0, 0, mazeBlockSize - 2, mazeBlockSize - 2);
        }
    }

    public class DrawAnimatedPlayer extends JPanel implements ActionListener
    {

        private Ellipse2D.Float ellipse = new Ellipse2D.Float();
        private double esize;
        private double maxSize = 0;
        private boolean initialize = true;
        Timer timer;
        ActionListener updateProBar;

        public void setXY(double size, int w, int h)
        {
            esize = size;
            ellipse.setFrameFromCenter(mazeBlockSize / 2, mazeBlockSize / 2, size, size);
        }

        public void reset(int w, int h)
        {
            maxSize = mazeBlockSize;
            setXY(maxSize * Math.random(), mazeBlockSize, mazeBlockSize);
        }

        public void step(int w, int h)
        {
            esize++;
            if (esize >= maxSize)
            {
                setXY(0, mazeBlockSize / 2, mazeBlockSize / 2);
            } else
            {
                //ellipse.setFrame(ellipse.getX(), ellipse.getY(), esize, esize);
                ellipse.setFrameFromCenter(mazeBlockSize / 2, mazeBlockSize / 2, esize, esize);
            }
        }

        public void actionPerformed(ActionEvent e)
        {
            repaint();
        }
        private BufferedImage image;

        public DrawAnimatedPlayer()
        {
            setXY(20 * Math.random(), mazeBlockSize, mazeBlockSize);

            timer = new Timer(20, this);
            timer.setInitialDelay(190);
            timer.start();

            try
            {
                image = ImageIO.read(new File("./Dungeon_Floor_1.png"));
            } catch (IOException ex)
            {
                System.out.println(ex);
                System.out.println("Working Directory = "
                        + System.getProperty("user.dir"));
            }
        }

        @Override
        public void paint(Graphics g)
        {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;

            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

            g2.setRenderingHints(rh);
            Dimension size = getSize();

            if (initialize)
            {
                reset(size.width, size.height);
                initialize = false;
            }
            this.step(size.width, size.height);

            g2.drawImage(scale(image, mazeBlockSize, mazeBlockSize), 0, 0, null);
            g2.setColor(Color.GREEN);
            g2.draw(ellipse);
        }
    }
}
