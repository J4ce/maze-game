
package mazegame;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import javax.swing.JPanel;


public class HoofdScherm extends javax.swing.JFrame {
    private int[][] maze;
    private int mazeContainerWidth;
    private int mazeContainerHeight;
    private Doolhof doolhof = new Doolhof();
    private Speler player;
    private Veld veld;
    private JPanel mazeContainer;
    
    public JPanel getMazeContainer() { return mazeContainer; }
    public Speler getSpeler() { return player; }
    
    /** Creates new form HoofdScherm */
    public HoofdScherm() {
        initComponents();
        initCustomComponents();
        this.setLocationRelativeTo(null);
        System.out.println(this.getName());
    }
    
    private void initCustomComponents(){
        //draw startscreen
        mazeContainer = this.mazePanel;
        mazeContainer.removeAll();
        mazeContainer.updateUI();
        mazeContainer.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;
        c.weighty = 0.5;
        mazeContainer.add(new Veld.StartScreen(), c);
        
        Rectangle r = mazeContainer.getBounds();
        mazeContainerWidth = r.width;
        mazeContainerHeight = r.height;
        
        //set up timed events:
//        class MyListener implements ActionListener 
//        { 
//            @Override
//            public void actionPerformed(ActionEvent event) 
//            { 
//                // Listener action (executed at each timer event) 
//            } 
//        } 
//        MyListener listener = new MyListener(); 
//        Timer t = new Timer(1000, listener);    // 1000 = every second 
//        t.start();                              // start event on timer
    }
    
    private void resizeMazeFrame(){
        this.mazePanel.revalidate();
        this.mazePanel.repaint();
        Rectangle e = this.mazePanel.getBounds();
        int wDelta = e.width - mazeContainerWidth;
        int hDelta = e.height - mazeContainerHeight;
        
        Dimension mazeFrame = getSize();
        int mFrameX = (wDelta > 0) ? mazeFrame.width + wDelta : mazeFrame.width;
        int mFrameY = (hDelta > 0) ? mazeFrame.height + hDelta : mazeFrame.height;
        this.setSize(mFrameX, mFrameY);
        
        mazeContainerWidth = e.width;
        mazeContainerHeight = e.height;
    }
    
    private void createNewMaze(int w, int h){
        maze = null;
        doolhof.generateMaze(w, h);
        maze = doolhof.getMaze();
        
        player = new Speler(doolhof);
        veld = new Veld(doolhof);
        
        //resize the blocks to build the maze with (small maze - large blocks, large maze - small blocks)
        veld.calcMazeBlockSize(w, h, mazePanel);
        
        //calculate window sizes for resizing after maze generation
        Rectangle windowMaze1 = this.mazePanel.getBounds();
        Rectangle windowFrame1 = this.getBounds();
        System.out.println(windowMaze1.toString());
        System.out.println(windowFrame1.toString());
    }
    
    private boolean solveMazeRecursively(int[][] mze, int x, int y, int d)
    {
        boolean ok = false;
        for (int i = 0; i < 4 && !ok; i++)
        {
            if (i != d)
            {
                switch (i)
                {
                    // 0 = up, 1 = right, 2 = down, 3 = left
                    case 0:
                        if (mze[y - 1][x] == 0)
                        {
                            ok = solveMazeRecursively(mze, x, y - 2, 2);
                        }
                        break;
                    case 1:
                        if (mze[y][x + 1] == 0)
                        {
                            ok = solveMazeRecursively(mze, x + 2, y, 3);
                        }
                        break;
                    case 2:
                        if (mze[y + 1][x] == 0)
                        {
                            ok = solveMazeRecursively(mze, x, y + 2, 0);
                        }
                        break;
                    case 3:
                        if (mze[y][x - 1] == 0)
                        {
                            ok = solveMazeRecursively(mze, x - 2, y, 1);
                        }
                        break;
                }
            }
        }
        // check for end condition
        if (x == 1 && y == 1)
        {
            ok = true;
        }
        // once we have found a solution, draw it as we unwind the recursion
        if (ok)
        {
            mze[y][x] = 9;
            switch (d)
            {
                case 0:
                    mze[y - 1][x] = 9;
                    break;
                case 1:
                    mze[y][x + 1] = 9;
                    break;
                case 2:
                    mze[y + 1][x] = 9;
                    break;
                case 3:
                    mze[y][x - 1] = 9;
                    break;
            }
        }
        return ok;
    }

    /**
     * Solve the maze and draw the solution. For simplicity,
     * assumes the starting point is the lower right, and the
     * ending point is the upper left.
     */
    private void solveMaze(int[][] mze)
    {
        solveMazeRecursively(mze, mze[0].length - 2, mze.length - 2, -1);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        mazePanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        lblXPosition = new javax.swing.JLabel();
        lblYPosition = new javax.swing.JLabel();
        lblSteps = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnGeefOp = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnStart = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Doolhof Spel");
        setBackground(new java.awt.Color(0, 0, 0));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("HoofdSchermMain");

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        mazePanel.setBackground(new java.awt.Color(0, 0, 0));
        mazePanel.setFocusCycleRoot(true);
        mazePanel.setName("mazeContainer");
        mazePanel.setPreferredSize(new java.awt.Dimension(780, 409));
        mazePanel.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                mazePanelComponentResized(evt);
            }
        });
        mazePanel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                mazePanelKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout mazePanelLayout = new javax.swing.GroupLayout(mazePanel);
        mazePanel.setLayout(mazePanelLayout);
        mazePanelLayout.setHorizontalGroup(
            mazePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 780, Short.MAX_VALUE)
        );
        mazePanelLayout.setVerticalGroup(
            mazePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 409, Short.MAX_VALUE)
        );

        jPanel5.setBackground(new java.awt.Color(0, 0, 0));
        jPanel5.setForeground(new java.awt.Color(255, 255, 255));

        lblXPosition.setBackground(new java.awt.Color(0, 0, 0));
        lblXPosition.setForeground(new java.awt.Color(255, 255, 255));
        lblXPosition.setText("x");
        lblXPosition.setToolTipText("");

        lblYPosition.setBackground(new java.awt.Color(0, 0, 0));
        lblYPosition.setForeground(new java.awt.Color(255, 255, 255));
        lblYPosition.setText("y");

        lblSteps.setBackground(new java.awt.Color(0, 0, 0));
        lblSteps.setForeground(new java.awt.Color(255, 255, 255));
        lblSteps.setText("steps:");
        lblSteps.setToolTipText("");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblXPosition, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                    .addComponent(lblYPosition, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSteps, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(69, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblXPosition)
                    .addComponent(lblSteps))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblYPosition)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("GAME BANNER");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 739, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(0, 0, 0));

        btnGeefOp.setBackground(new java.awt.Color(255, 0, 0));
        btnGeefOp.setFont(new java.awt.Font("Bradley Hand ITC", 1, 14)); // NOI18N
        btnGeefOp.setText("GEEF OP");
        btnGeefOp.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 6));
        btnGeefOp.setBorderPainted(false);
        btnGeefOp.setDefaultCapable(false);
        btnGeefOp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeefOpActionPerformed(evt);
            }
        });

        btnReset.setBackground(new java.awt.Color(255, 153, 0));
        btnReset.setFont(new java.awt.Font("Bradley Hand ITC", 1, 14)); // NOI18N
        btnReset.setText("RESET");
        btnReset.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 6));
        btnReset.setBorderPainted(false);
        btnReset.setDefaultCapable(false);
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        btnStart.setBackground(new java.awt.Color(51, 204, 0));
        btnStart.setFont(new java.awt.Font("Bradley Hand ITC", 1, 14)); // NOI18N
        btnStart.setText("START");
        btnStart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 6));
        btnStart.setBorderPainted(false);
        btnStart.setDefaultCapable(false);
        btnStart.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGeefOp, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnStart, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(btnReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGeefOp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(mazePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mazePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mazePanel.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnStartActionPerformed
    {//GEN-HEADEREND:event_btnStartActionPerformed
        //create maze:
        int w = 51;
        int h = 25;
        createNewMaze(w, h);
        solveMaze(maze);
        //initialize Speler:
        player.reset(1, 1);
        
        //initialize statusboard
        lblXPosition.setText("x: " + player.getCurrentPosX());
        lblYPosition.setText("y: " + player.getCurrentPosY());
        lblSteps.setText("steps: " + player.getStepsTaken());
        
        //draw player in maze:
        veld.drawMaze();
        
        //prepare maze for keyboard input:
        this.mazePanel.requestFocus();
    }//GEN-LAST:event_btnStartActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnResetActionPerformed
    {//GEN-HEADEREND:event_btnResetActionPerformed
        //initialize statusboard
        lblXPosition.setText("x: " + player.getCurrentPosX());
        lblYPosition.setText("y: " + player.getCurrentPosY());
        lblSteps.setText("steps: " + player.getStepsTaken());
        
        //reset maze by copying from original maze
        doolhof.resetMaze();
        maze = doolhof.getMaze();
        
        //initialize Speler:
        player.reset(1, 1);
        
        //draw player in maze:
        veld.drawMaze();
        
        //prepare maze for keyboard input:
        this.mazePanel.requestFocus();
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnGeefOpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnGeefOpActionPerformed
    {//GEN-HEADEREND:event_btnGeefOpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGeefOpActionPerformed

    private void mazePanelComponentResized(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_mazePanelComponentResized
    {//GEN-HEADEREND:event_mazePanelComponentResized
        //in case the gridbaglayout resizes the jpanel:
        try{
            JPanel mf = mazePanel;
            int w = maze[0].length;
            int h = maze.length;

            Rectangle r = mf.getBounds();
            int mw = (int) Math.round(r.width / w) - 1;
            int mh = (int) Math.round(r.height / h) - 1;
            System.out.format("mw = %3d, mh = %3d\n", mw, mh);
            veld.setMazeBlockSize(Math.min(mw, mh));
            System.out.println("PaintMaze.mazeBlockSize = " + veld.getMazeBlockSize());
        }
        catch(Exception e){
            System.out.println("");
        }

        resizeMazeFrame();
    }//GEN-LAST:event_mazePanelComponentResized

    private void mazePanelKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_mazePanelKeyPressed
    {//GEN-HEADEREND:event_mazePanelKeyPressed
        System.out.print("KeyPressed: " + evt.getKeyCode() + "   ");
        // check if an arrow key was pressed
        if(evt.isActionKey()){
            int kc = evt.getKeyCode();
            switch(kc){
                case KeyEvent.VK_DOWN : 
                    //move_down();
                    player.move_player(Move.DOWN);
                    veld.updateMaze(player);
                    System.out.println("PIJL OMLAAG!!");
                    break;
                case KeyEvent.VK_UP : 
                    //move_up();
                    player.move_player(Move.UP);
                    veld.updateMaze(player);
                    System.out.println("PIJL OMHOOG!!");
                    break;
                case KeyEvent.VK_LEFT : 
                    //move_left();
                    player.move_player(Move.LEFT);
                    veld.updateMaze(player);
                    System.out.println("PIJL LINKS!!");
                    break;
                case KeyEvent.VK_RIGHT : 
                    //move_right();
                    player.move_player(Move.RIGHT);
                    veld.updateMaze(player);
                    System.out.println("PIJL RECHTS!!");
                    break;
            }
        }
        lblXPosition.setText("x: " + player.getCurrentPosX());
        lblYPosition.setText("y: " + player.getCurrentPosY());
        lblSteps.setText("steps: " + player.getStepsTaken());
    }//GEN-LAST:event_mazePanelKeyPressed
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HoofdScherm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HoofdScherm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HoofdScherm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HoofdScherm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HoofdScherm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeefOp;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblSteps;
    private javax.swing.JLabel lblXPosition;
    private javax.swing.JLabel lblYPosition;
    private javax.swing.JPanel mazePanel;
    // End of variables declaration//GEN-END:variables

}
