package mazegame;


public enum Item
{
    //values in maze array represent:
    PATH(0),
    WALL(1),
    SPELER(2),
    BAZOOKA(3),
    HELPER(4),
    VALSSPELER(5),
    PATH_TO_EXIT(6),
    ROCKET(7),
    VRIEND(8);

    private int itemValue = 0;

    private Item(int itemVal)
    {
        this.itemValue = itemVal;
    }
    
    public int getItemValue()
    {
        return itemValue;
    }
}
