package mazegame;

import java.awt.Point;

public class Speler extends VeldItem
{
    private Doolhof doolhof;
    //count moves made by the player
    private int stepsTaken = 0;

    public Speler(Doolhof doolhof)
    {
        //put player in left corner of maze:
        super(doolhof, Item.SPELER, 1, 1);
        this.doolhof = doolhof;
    }
    
    public void move_player(Move m)
    {
        Point p = this.getCurrentPos();
        int x = doolhof.getWidth();//maze[0].length;
        int y = doolhof.getHeight();//maze.length;
        
        //check if player is not moving out of the maze:
        boolean move = false;
        if(m == Move.UP && p.y + m.getY() > 0) move = true;
        if(m == Move.DOWN && p.y + m.getY() < y) move = true;
        if(m == Move.LEFT && p.x + m.getX() > 0) move = true;
        if(m == Move.RIGHT && p.x + m.getX() < x) move = true;
        
        int maze[][] = doolhof.getMaze();
        if(move){
            if(maze[p.y + m.getY()][p.x + m.getX()] != Item.WALL.getItemValue()){    //0 = path, 1 = wall
                this.setCurrentPos(p.x + m.getX(), p.y + m.getY());
                stepsTaken++;
            }
        }
    }
    
    public int getStepsTaken()
    {
        return stepsTaken;
    }
    
    public void setStepsTaken(int steps)
    {
        stepsTaken = steps;
    }
    
    @Override
    public void reset(int x, int y)
    {
        super.reset(x, y);
        stepsTaken = 0;
    }
    
    public static void print(Speler p){
        System.out.format("player: cx=%d ,cy=%d   px=%d ,py=%d\n", p.getCurrentPosX(), p.getCurrentPosY(), p.getPrevPosX(), p.getPrevPosY());
    }
}
