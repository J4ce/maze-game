package mazegame;


public enum Move
{
    //move directions on 2d field, based on x and y position:
    UP(0, -1),
    DOWN(0, 1),
    LEFT(-1, 0),
    RIGHT(1, 0);
    
    private int x = 0;
    private int y = 0;

    private Move(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public int getX()
    {
        return x;
    }
    
    public int getY()
    {
        return y;
    }
}
