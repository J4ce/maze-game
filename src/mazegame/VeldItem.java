package mazegame;

import java.awt.Point;


public class VeldItem
{
    //currrent position:
    private int currentPosX = 0;
    private int currentPosY = 0;
    private Point currentPos = new Point();
    //previous position:
    private int prevPosX = 0;
    private int prevPosY = 0;
    private Point prevPos = new Point();
    
    private Item item;
    private Doolhof doolhof;
    
    public VeldItem(Doolhof doolhof, Item item, int x, int y)
    {
        this.doolhof = doolhof;
        this.item = item;
        
        prevPosX = x;
        prevPosY = y;
        prevPos.setLocation(prevPosX, prevPosY);
        
        currentPosX = x;
        currentPosY = y;
        currentPos.setLocation(currentPosX, currentPosY);
        updatePositionInModel(item);
    }

    public int getCurrentPosX() { return currentPosX; }

    public void setCurrentPosX(int x)
    {
        if (x != currentPosX)
        {
            prevPosX = currentPosX;
            prevPosY = currentPosY;
            prevPos.setLocation(prevPosX, prevPosY);
            
            currentPosX = x;
            currentPos.setLocation(currentPosX, currentPosY);
            updatePositionInModel(item);
        }
    }

    public int getCurrentPosY() { return currentPosY; }

    public void setCurrentPosY(int y)
    {
        if (y != currentPosY)
        {
            prevPosX = currentPosX;
            prevPosY = currentPosY;
            prevPos.setLocation(prevPosX, prevPosY);
            
            currentPosY = y;
            currentPos.setLocation(currentPosX, currentPosY);
            updatePositionInModel(item);
        }
    }

    public Point getCurrentPos() { return currentPos; }

    public void setCurrentPos(int x, int y)
    {
        //System.out.format("setCurrentPos x=%d, y=%d, new x=%d, new y=%d\n", currentPosX, currentPosY, x, y);
        if (currentPosX != x || currentPosY != y)
        {
            prevPosX = currentPosX;
            prevPosY = currentPosY;
            prevPos.setLocation(prevPosX, prevPosY);
            
            currentPosX = x;
            currentPosY = y;
            currentPos.setLocation(currentPosX, currentPosY);
            updatePositionInModel(item);
        }
    }

    public Point getPrevPos() { return prevPos; }
    public int getPrevPosX() { return prevPosX; }
    public int getPrevPosY() { return prevPosY; }
    
    public void reset(int x, int y)
    {
        prevPosX = x;
        prevPosY = y;
        currentPosX = x;
        currentPosY = y;
        currentPos.setLocation(currentPosX, currentPosY);
        prevPos.setLocation(prevPosX, prevPosY);
        updatePositionInModel(item);
    }
    
    private void print(VeldItem vi){
        System.out.format("%s: cx=%d ,cy=%d   px=%d ,py=%d\n", item.toString(), vi.getCurrentPosX(), vi.getCurrentPosY(), vi.getPrevPosX(), vi.getPrevPosY());
    }
    
    //update maze array (model) with VeldItem location:
    private void updatePositionInModel(Item item)
    {
        int[][] maze = doolhof.getMaze();
        int[][] maze_original = doolhof.getMaze_Original();
        if(maze != null)
        {
            int px = this.getPrevPosX();
            int py = this.getPrevPosY();

            int cx = this.getCurrentPosX();
            int cy = this.getCurrentPosY();

            //replace previous position in maze with original value
            maze[py][px] = maze_original[py][px];
            //replace current position in maze wih VeldItem value
            maze[cy][cx] = item.getItemValue();
        }
        this.print(this);
    }
}
