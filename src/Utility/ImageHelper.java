/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.UIManager;

/**
 *
 * @author John
 */
public final class ImageHelper {

    public static BufferedImage ScaleImage(BufferedImage src, int w, int h) {
        BufferedImage thempBufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D thempGraphics2D = thempBufferedImage.createGraphics();

        thempGraphics2D.setBackground(UIManager.getColor("Panel.background"));
        thempGraphics2D.clearRect(0, 0, w, h);

        double xScale = (double) w / src.getWidth();
        double yScale = (double) h / src.getHeight();
        double scale = Math.min(xScale, yScale);

        int width = (int) (scale * src.getWidth());
        int height = (int) (scale * src.getHeight());
        int x = (w - width) / 2;
        int y = (h - height) / 2;
        thempGraphics2D.drawImage(src, x, y, width, height, null);
        thempGraphics2D.dispose();

        return thempBufferedImage;
    }
}
