/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author John
 */
public class Player extends JPanel implements Runnable {
    
    private Thread Thread_Runner;
    private int SLEEP_TIME = 500;
        
    public int mazeBlockSize = 25;
    
    private static final Color PLAYER = Color.GREEN;
    private static final Color PLAYER2 = Color.RED;
    public Color mazeBlockColor = Color.BLACK;

    public Player()
    {
        System.out.println("Constructor");
    }
    
    public void start() {
        if (Thread_Runner == null) {
            Thread_Runner = new Thread(this);
            Thread_Runner.start();
        }
    }

    public void stop() {
        if (Thread_Runner != null) {
            Thread_Runner = null;
        }
    }

    @Override
    public void run() {
        Thread thisThread = Thread.currentThread();
        while (Thread_Runner == thisThread) {
            
            if(mazeBlockColor != PLAYER)
                mazeBlockColor = PLAYER;
            else
                mazeBlockColor = PLAYER2;
            
            repaint();
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(mazeBlockColor);
        g2d.drawRect(0, 0, mazeBlockSize, mazeBlockSize);
        g2d.setColor(mazeBlockColor);
        g2d.fillRect(1, 1, mazeBlockSize - 2, mazeBlockSize - 2);
    }
}
